from cantpy import cant

CAN_PROTOCOL_FILENAME = "cantid"

print("Start!")


# print("test:")
# aa=b"321"
# ss=ctypes.c_char_p(b"CABB")
# cant().test(b"bbb")
# cant().test(aa)
# cant().test(ss)
C = cant()
print("\n ++++++++++++++++++++++++++++++++++++++++++++++++ \n")
print("Begin TEST \n")

print("getIdAttrSize=", C.getIdAttrSize())
print("getLoadId=", C.getLoadId())
print("Step1 !")
print("setAttrByNdx=", C.setAttrByNdx(1, 4))
print("setAttrByName=", C.setAttrByName(b"Sender", 2))

print("4: getAttrByNdx=", C.getAttrByNdx(1))
print("2: getAttrByName=", C.getAttrByName(b"Sender"))
# print("getAttrByName=", cant().getAttrByName(b"Attribute"))

print("\n Step2 !")
print("setAttrByNdx=", C.setAttrByNdx(0, 1))
print("setAttrByNdx=", C.setAttrByNdx(1, 22))
print("setAttrByNdx=", C.setAttrByNdx(2, 111))
print("setAttrByNdx=", C.setAttrByNdx(3, 3))

print("setAttrByNdx=", C.setAttrByNdx(4, 23))
print("setAttrByNdx=", C.setAttrByNdx(5, 2))
print("setAttrByNdx=", C.setAttrByNdx(6, 24))
print("setAttrByNdx=", C.setAttrByNdx(7, 1))

C.print_frame()

print("Step2 OK")

print("Step code !")
print("codeId()      ", C.codeId())
print("decodeId()    ", C.decodeId())
print("getAdr()    ", C.getAdr())

print("Step3 ! Structure Test!")
C.ClearIdStructure()
print("getIdAttrSize=", C.getIdAttrSize())
C.AddIdStruct("Test", 12, True)
C.InitId()
print("getIdAttrSize=", C.getIdAttrSize())


print("Step4 !")


#
print("getIdAttrSize=", C.getIdAttrSize())

Ns = C.getIdAttrSize()

for i in range(0, Ns):
    print("i=", i, " MaxVal=", C.getIdAttrMaxValByNdx(i))
# print(len(StrName))

# for i in range(len(StrName)) :
#   print(StrName[i]," ",StrLen[i])
#    cant().AddIdStruct(StrName[i], StrLen[i])

# cant().InitId()
# print("getIdAttrSize=", cant().getIdAttrSize())


print("Finish!")
