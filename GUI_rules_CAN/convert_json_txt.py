# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import argparse
import json


def main():
    parser = argparse.ArgumentParser(description="Convert json txt")

    parser.add_argument(
        "-i",
        type=str,
        help="путь до json файла",
        dest="infile",
        default="/Users/nikitakamenev/Documents/scince/gitlab/flow (6).json",
    )

    parser.add_argument(
        "-o",
        type=str,
        help="путь до txt файла",
        dest="out",
        default="/Users/nikitakamenev/Documents/scince/gitlab/out_file.txt",
    )

    args = parser.parse_args()

    name_txt = ""
    id_name_can = {}
    source_id = 0
    count_str = 0
    bottom_list = []

    with open(args.infile, "r") as json_file:
        data_js = json.load(json_file)

    for data in data_js:
        if "source" in data.keys():
            source_id = data["source"]
            break

    for data in data_js:
        if "data" in data.keys():
            id_name_can[data["id"]] = data["data"]["label"]

    source_name = id_name_can[source_id].split()

    name_txt += f"{source_name[2]}\n{source_name[3]}\n{source_name[4]}\n"

    for id_name in id_name_can.keys():
        if id_name != source_id:
            count_str += 1
            id_name_can_sp = id_name_can[id_name].split()[1]
            name_count = (
                f"0 {id_name_can[source_id].split()[1]} 0 {id_name_can_sp}"
                f" 0 {id_name_can_sp} 0 {id_name_can[source_id].split()[1]}"
            )

            bottom_list.append(name_count)

    name_txt += f"{str(count_str)}\n"

    for bottom in bottom_list:
        name_txt += f"{bottom}\n"

    with open(args.out, "w") as f:
        f.write(name_txt)


if __name__ == "__main__":
    main()
