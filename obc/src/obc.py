# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

# %%
import os

# TODO написать обертку для модуля, которая работает с шиной CAN, чтобы не надо было
# делать условный импорт и можно было вызывать файл без шины CAN и из других скриптов,
# например, для того, чтобы запустить моделирование вообще без шины CAN.
if __name__ == "__main__":
    import can

from config.mission_parameters import (
    FINAL_FUEL_MASS,
    MACHINE_ZERO,
    OBC_ID,
    SIMULATOR_ID,
)
from libs.ballistic import sat_is_in_thrusting_allowed_zone, sun_visibility
from libs.can_utils import (
    collect_str_from_can_bus,
    send_args_to_can_bus,
    unpack_args_from_str,
)


# %%
def calc_new_sat_state(t, y, cur_sat_state, strategy):
    m_sum = y[1]
    r_vector = y[2:5]

    sun_vis = sun_visibility(t, r_vector)

    if cur_sat_state == "initial":
        if sun_vis > 0.5:
            new_sat_state = "heat_accumulation"
        else:
            new_sat_state = "eclipse"

    elif cur_sat_state == "heat_accumulation":
        if abs(sun_vis - 0.5) < MACHINE_ZERO:
            new_sat_state = "eclipse"
        else:
            if sat_is_in_thrusting_allowed_zone(y, strategy):
                new_sat_state = "orbital_orientation"
            else:
                new_sat_state = "waiting_thrusting_allowed_zone"

    elif cur_sat_state in (
        "orbital_orientation",
        "waiting_thrusting_allowed_zone",
    ):
        new_sat_state = "thrusting"

    elif cur_sat_state == "eclipse":
        new_sat_state = "heat_accumulation"

    elif cur_sat_state == "thrusting":
        if m_sum > FINAL_FUEL_MASS:
            new_sat_state = "eclipse" if sun_vis < 0.5 else "heat_accumulation"
        else:
            new_sat_state = "out_of_fuel"

    elif cur_sat_state == "out_of_fuel":
        new_sat_state = cur_sat_state  # to avoid error at simulation end

    else:
        raise Exception("Unknown satellite state")

    return new_sat_state


if __name__ == "__main__":
    CAN_SERVER_HOST = os.getenv("CAN_SERVER_HOST")
    bus = can.interface.Bus(
        f"ws://{CAN_SERVER_HOST}:54701/", bustype="remote", bitrate=500000
    )
    while True:
        whole_msg = collect_str_from_can_bus(arbitration_id=SIMULATOR_ID, bus=bus)
        if whole_msg is not None:
            t, y, cur_sat_state, strategy = unpack_args_from_str(whole_msg)
            new_sat_state = calc_new_sat_state(t, y, cur_sat_state, strategy)
            send_args_to_can_bus(new_sat_state, arbitration_id=OBC_ID, bus=bus)
