# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
from time import sleep

import cv2
from loguru import logger
from omegaconf import OmegaConf
from picamera import PiCamera
from src.classification_planet import PlanetClassifier

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path_model = os.path.join(BASE_DIR, "weights/bestmodel.tflite")

cfg = OmegaConf.load("config/config.yml")

parser = argparse.ArgumentParser()
parser.add_argument(
    "-i",
    "--image",
    help="path to the input image",
    default=os.path.join(BASE_DIR, "tests", "fixtures", "images", "file_20.jpg"),
)
args = parser.parse_args()


def capture_image_from_camera():
    camera = PiCamera()
    camera.start_preview()

    sleep(2)
    camera.capture("data/capture.jpg")
    camera.stop_preview()
    camera.close()

    if os.path.exists("data/capture.jpg"):
        return cv2.imread("data/capture.jpg", cv2.COLOR_BGR2RGB)
    else:
        logger.debug("Failed to capture image from camera")


def predict(image):
    try:
        model = PlanetClassifier(cfg)

        predict = model.predict_proba(image)
        response = {"predict": predict}

        return response

    except Exception:
        logger.debug("Image not found")


if __name__ == "__main__":
    if cv2.VideoCapture(0).isOpened():
        image = capture_image_from_camera()
    else:
        image = cv2.imread(args.image, cv2.COLOR_BGR2RGB)

    if image is not None:
        pred = predict(image)
        print(pred)
        with open("data/result.txt", "w") as file:
            file.write(pred["predict"])
    else:
        logger.debug("Failed to capture image")
