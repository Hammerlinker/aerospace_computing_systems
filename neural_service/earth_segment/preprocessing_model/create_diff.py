import argparse
import os

import h5py
from loguru import logger

path_oldmodel = os.path.join("weights/bestmodel.h5")
path_diff = os.path.join("weights/param_diff.h5")

out_model = "weights/update_new_model.h5"

parser = argparse.ArgumentParser()
parser.add_argument(
    "-o",
    "--old_model",
    help="path to the old model",
    default=path_oldmodel,
)

parser.add_argument(
    "-d",
    "--diff_model",
    help="path to the diff model",
    default=path_diff,
)

parser.add_argument(
    "-n",
    "--new_model",
    help="path to the new model",
    default=out_model,
)
args = parser.parse_args()


def compute_param_diff_h5(old_model, new_model):
    with h5py.File(old_model, "r") as old_model, h5py.File(new_model, "r") as new_model:
        param_diff = {}
        for old_param_name, old_param in old_model.items():
            if isinstance(old_param, h5py.Dataset):
                new_param = new_model[old_param_name]

                diff = old_param[()] - new_param[()]
                param_diff[old_param_name] = diff

    return param_diff


def save_param_diff_h5(param_diff, output_file_path):
    with h5py.File(output_file_path, "w") as output_file:
        for param_name, diff in param_diff.items():
            output_file.create_dataset(param_name, data=diff)


if __name__ in "__main__":
    param_diff = compute_param_diff_h5(args.old_model, args.new_model)
    save_param_diff_h5(param_diff, args.diff_model)
    logger.debug("Create diff model")
