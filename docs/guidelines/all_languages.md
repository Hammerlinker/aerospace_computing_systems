# Общие рекомендации для всех языков

Все указанные ниже расширения могут быть установлены автоматически по этой
[инструкции](../../contributing.md#2-использование-автоматических-инструментов-ide)

1. Строки комментариев должны укладываться в длину строки 88 символов (первая
   вертикальная линия). Для автоматического перестроения блока комментариев удобно
   использовать добавленное в рекомендации для проекта расширение
   [rewrap](https://marketplace.visualstudio.com/items?itemName=stkb.rewrap). Можно
   выделить весь текст (Ctrl+A) и нажать Alt+Q. Код останется без изменений, а
   комментарии будут реструктурированы. Можно изменять комментарии и в отдельной части
   документа или переформатировать строку, в которой находится курсор.

<!-- ![Нажмите Alt+Q](https://stkb.github.io/Rewrap/images/example.svg) -->

1. Опечатки в словах проверяются с помощью расширения [Code Spell
   Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
   и расширения со словарём русского языка [Russian - Code Spell
   Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-russian).
   Есть возможность добавлять слова в исключения workspace, исключения сохранятся в
   файле [`.vscode/settings.json`](../../.vscode/settings.json) и при сохранении в
   репозитории будут актуализированы у всех пользователей.
